/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class Datatourisme {

    public static final Resource resource(String suffix )
    { return ResourceFactory.createResource( NS + suffix ); }

    public static final Property property(String suffix )
    { return ResourceFactory.createProperty( NS + suffix ); }

    /** <p>The namespace of the vocabulary as a string ({@value})</p> */
    public static final String NS = "https://www.datatourisme.gouv.fr/ontology/core#";

    /** <p>The namespace of the vocabulary as a string</p>
     *  @see #NS */
    public static String getURI() {return NS;}

    public static final String PREFIX = "";

    /** <p>The namespace of the vocabulary as a resource</p> */
    public static final Resource NAMESPACE = resource( NS );

    // Vocabulary classes
    ///////////////////////////
    public static final Resource PointOfInterest = resource( "PointOfInterest" );
    public static final Resource PointOfInterestClass = resource( "PointOfInterestClass" );

    public static final Resource PlaceOfInterest = resource( "PlaceOfInterest" );
    public static final Resource EntertainmentAndEvent = resource( "EntertainmentAndEvent" );
    public static final Resource Product = resource( "Product" );
    public static final Resource Tour = resource( "Tour" );

    // Vocabulary properties
    ///////////////////////////
    public static final Property isLocatedAt = property( "isLocatedAt" );
    public static final Property hasAddressCity = property( "hasAddressCity" );
    public static final Property latlon = property( "latlon" );
    public static final Property hasPriority = property( "hasPriority" );
    public static final Property hasBeenCreatedBy = property( "hasBeenCreatedBy" );
    public static final Property hasBeenPublishedBy = property( "hasBeenPublishedBy" );
    public static final Property lastUpdateDatatourisme = property( "lastUpdateDatatourisme" );
}
