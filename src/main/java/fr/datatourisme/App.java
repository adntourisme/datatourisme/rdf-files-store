/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme;

import com.conjecto.graphstore.*;
import com.conjecto.graphstore.exception.GraphStoreException;
import fr.datatourisme.jsonld.JsonLdSerializer;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.vocabulary.DatatourismeMetadata;
import fr.datatourisme.vocabulary.DatatourismeThesaurus;
import fr.datatourisme.xml.XmlSerializer;
import org.apache.commons.cli.*;
import org.apache.jena.base.Sys;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;

import java.io.*;
import java.util.*;

/**
 * JsonLdStoreApp
 */
public class App
{
    static String DEFAULT_NAMESPACE = "https://www.datatourisme.gouv.fr/ontology/core#";

    public static void main( String[] args ) throws IOException {
        CommandLineParser parser = new DefaultParser();

        Options options = new Options();
        options.addOption( null, "timeout", true, "define the timeout to acquire the store lock ." );
        options.addOption( "o", "ontology", true, "ontology file(s) to use for context generation ." );

        try {
            // parse the command line arguments
            CommandLine cmd = parser.parse( options, args );

            List<String> argList = cmd.getArgList();
            if(argList.size() < 2) {
                printHelp(options);
                System.exit(0);
            }

            File input = new File(argList.get(0));
            File output = new File(argList.get(1));

            // check output directory is empty
            if (output.exists()) {
                if (output.listFiles().length > 0) {
                    throw new RuntimeException("The output directory is not empty.");
                }
            } else {
                output.mkdirs();
            }

            // open the graphstore
            GraphStore store = GraphStore.openReadOnly(input.getAbsolutePath());

            // build context
            ContextBuilder ctxBuilder = new ContextBuilder(DEFAULT_NAMESPACE);
            for (String file : cmd.getOptionValues("o")) {
                ctxBuilder.addOntology(file.trim());
            }
            Context context = ctxBuilder
                .excludeNamespace(OWL.NS)
                .excludeNamespace(DatatourismeMetadata.NS)
                .addNamespace(DatatourismeThesaurus.PREFIX, DatatourismeThesaurus.NS)
                .build();

            // write context json
            File contextFile = new File(output, "context.jsonld");
            BufferedWriter writer = new BufferedWriter(new FileWriter(contextFile));
            writer.write(context.toJson());
            writer.close();

            // write poi files
            JsonLdSerializer jsonSerializer = new JsonLdSerializer(store, context);
            XmlSerializer xmlSerializer = new XmlSerializer(store, context);
            TripletIterator iter = store.querySPO();

            iter.stream().parallel().forEach(triplet -> {
                if (triplet.getPredicate().getLabel().equals(RDF.type.getURI()) && triplet.getObject().getLabel().equals(Datatourisme.PointOfInterest.getURI())) {
                    try {
                        String uri = triplet.getSubject().getLabel();
                        System.out.println(uri);
                        String uuid = uri.substring(uri.lastIndexOf('/') + 1);

                        // prepare directory
                        File directory = new File(output, getFileSubPath(uuid, 2));
                        directory.mkdirs();

                        // JSON
                        jsonSerializer.serialize(triplet.getSubject(), new File(directory, uuid + ".json"), output);
                        // XML
                        xmlSerializer.serialize(triplet.getSubject(), new File(directory, uuid + ".xml"), output);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            store.close();
        }
        catch(ParseException exp ) {
            // oops, something went wrong
            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
        }
        catch(GraphStoreException exp ) {
            // oops, something went wrong
            System.err.println( "Graphstore reading failed.  Reason: " + exp.getMessage() );
        }
    }

    /**
     * @param uuid
     * @return
     */
    private static String getFileSubPath(String uuid, int depth) {
        LinkedList<String> path = new LinkedList<>();
        while (depth > 0) {
            path.addFirst(uuid.substring(0,depth));
            depth--;
        }
        return String.join(File.separator, path);
    }

    /**
     * @param options
     */
    private static void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        String header = "\nDump a files store from DATAtourisme Graphstore\n\n<input> : Path to source Graphstore (required)\n<output> : Path to output directory (required)\n\n";
        formatter.printHelp("java -jar rdf-files-store.jar <input> <output>", header, options, null, true);
    }

}
