/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.jsonld;

import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.PrefixMapping;
import com.conjecto.graphstore.Triplet;
import com.conjecto.graphstore.TripletIterator;
import com.conjecto.graphstore.iterator.PredicateIterator;
import com.google.gson.stream.JsonWriter;
import fr.datatourisme.Context;
import fr.datatourisme.Serializer;
import fr.datatourisme.vocabulary.Datatourisme;
import org.apache.jena.vocabulary.RDF;
import org.semanticweb.yars.nx.BNode;
import org.semanticweb.yars.nx.Literal;
import org.semanticweb.yars.nx.Node;
import org.semanticweb.yars.nx.Resource;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class JsonLdSerializer extends Serializer {
    public JsonLdSerializer(GraphStore store, Context context) {
        super(store, context);
    }

    public void serialize(Node node, OutputStream out) throws IOException {
        JsonWriter writer = new JsonWriter(new OutputStreamWriter(out));
        writer.setIndent("  ");
        writeResource(writer, node, null);
        writer.flush();
    }

    /**
     * @param writer
     * @param node
     * @param root
     * @throws IOException
     */
    protected void writeResource(JsonWriter writer, Node node, Node root) throws IOException {
        writer.beginObject();
        writeNodeReferenceId(writer, node);
        if (!node.equals(root)) { // avoid cyclic dependencies
            PredicateIterator iterator = new PredicateIterator(store.querySPO(node.toString()));
            while(iterator.hasNext()) {
                writePredicate(writer, iterator.next(), root != null ? root : node);
            }
        }
        writer.endObject();
    }

    /**
     * @param writer
     * @param iteration
     */
    protected void writePredicate(JsonWriter writer, PredicateIterator.PredicateIteration iteration, Node root) throws IOException {

        List<Triplet> triplets = new ArrayList<>();
        while(iteration.iterator.hasNext()) {
            Triplet triplet = iteration.iterator.next();
            if (context.isExclude(triplet)) {
                // if the triplet contain excluded namespace, ignore it
                continue;
            }
            triplets.add(triplet);
        }

        if (triplets.size() == 0) {
            return;
        }

        Resource predicate = iteration.predicate;
        switch(predicate.getLabel()) {
            case "http://www.w3.org/1999/02/22-rdf-syntax-ns#type":
                writer.name("@type");
                break;
            default:
                writer.name(shortForm(predicate.getLabel()));
        }

        Triplet first = triplets.get(0);
        if (first.getObject() instanceof Literal && ((Literal) first.getObject()).getLanguageTag() != null) {
        // if (first.getObject() instanceof Literal && context.hasLangStringRange(predicate.getLabel())) {
            // specific : languages
            Map<String, List<Triplet>> map = triplets.stream()
                .filter(triplet -> triplet.getObject() instanceof Literal)
                .collect(Collectors.groupingBy(triplet -> ((Literal) triplet.getObject()).getLanguageTag() != null ? ((Literal) triplet.getObject()).getLanguageTag() : context.getDefaultLanguage()));
            writer.beginObject();
            for (Map.Entry<String, List<Triplet>> entry : map.entrySet()) {
                writer.name(entry.getKey());
                writePredicateValues(writer, entry.getValue(), root);
            }
            writer.endObject();
            return;
        }

        // else
        writePredicateValues(writer, triplets, root);
    }

    /**
     * @param writer
     * @param triplets
     */
    protected void writePredicateValues(JsonWriter writer, List<Triplet> triplets, Node root) throws IOException {
        if (triplets.size() > 1) {
            writer.beginArray();
        }

        for (Triplet triplet : triplets) {
            Node node = triplet.getObject();
            if(node instanceof Resource || node instanceof BNode) {
                if (context.isEmbeddable(triplet)) {
                    writeResource(writer, node, root);
                } else {
                    writer.value(shortForm(node.getLabel()));
                }
            } else if (node instanceof Literal) {
                writeLiteral(writer, (Literal) node);
            }
        }

        if (triplets.size() > 1) {
            writer.endArray();
        }
    }

    /**
     * @param writer
     * @param node
     * @throws IOException
     */
    protected void writeNodeReferenceId(JsonWriter writer, Node node) throws IOException {
        if(node instanceof BNode) {
            writer.name("@id").value("_:" + node.getLabel());
        } else {
            writer.name("@id").value(shortForm(node.getLabel()));
        }
    }

    /**
     * @param writer
     * @param literal
     * @throws IOException
     */
    protected void writeLiteral(JsonWriter writer, Literal literal) throws IOException {
        if(literal.getDatatype() != null) {
            String value = literal.getLabel();
            switch(literal.getDatatype().getLabel()) {
                case "http://www.w3.org/2001/XMLSchema#int":
                case "http://www.w3.org/2001/XMLSchema#integer":
                    writer.value(Integer.valueOf(value));
                    break;
                case "http://www.w3.org/2001/XMLSchema#double":
                case "http://www.w3.org/2001/XMLSchema#decimal":
                    writer.value(Double.valueOf(value));
                    break;
                case "http://www.w3.org/2001/XMLSchema#boolean":
                    writer.value(Boolean.valueOf(value));
                    break;
                default:
                    writer.value(value);
            }
        } else {
            writer.value(literal.getLabel());
        }
    }

    /**
     * @param uri
     * @return
     */
    protected String shortForm(String uri) {

        // #1 : test @vocab
        if(context.getDefaultNamespace() != null) {
            String namespace = PrefixMapping.extractNamespace(uri);
            if(context.getDefaultNamespace().equals(namespace)) {
                return uri.substring(namespace.length());
            }
        }

        // #3 else, use prefix mapping
        return context.getPrefixMapping().shortForm(uri);
    }

    /**
     * @param node
     * @param out
     * @throws IOException
     */
    public void serializeIndexEntry(Node node, OutputStream out, Path filePath) throws IOException {
        JsonWriter writer = new JsonWriter(new SpecialOutputStreamWriter(out));
        writer.setIndent("  ");
        writer.beginObject();

        // uuid, file
        String uri = node.getLabel();
        writer.name("uuid").value(uri.substring(uri.lastIndexOf('/') + 1));

        // label
        String label = getFirstValue(node, "http://www.w3.org/2000/01/rdf-schema#label");
        if (label != null) {
            writer.name("label").value(label);
        }

        // lastUpdate
        String lastUpdate = getFirstValue(node, Datatourisme.lastUpdateDatatourisme.getURI());
        if (label != null) {
            writer.name("lastUpdate").value(lastUpdate);
        }

        // file
        writer.name("file").value(filePath.toString());

        writer.endObject();
        writer.flush();
    }

    /**
     * Special class used to add extra indentation to JSON
     */
    static class SpecialOutputStreamWriter extends OutputStreamWriter {
        boolean dirty = false;

        public SpecialOutputStreamWriter(OutputStream outputStream) {
            super(outputStream);
        }

        @Override
        public void write(String s, int i, int i1) throws IOException {
            if (!dirty) {
                dirty = true;
                write("  ");
            }
            super.write(s, i, i1);
            if (s.equals("\n")) {
                write("  ");
            }
        }
    }
}
