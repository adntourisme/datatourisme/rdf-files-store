/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.*;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFParser;

public class ContextBuilder {
    OntModel model;
    String defaultNamespace;
    List<String> excludedNamespaces;
    Map<String, String> additionalNamespaces;

    public ContextBuilder(String defaultNamespace) {
        this.defaultNamespace = defaultNamespace;
        this.excludedNamespaces = new ArrayList<>();
        this.additionalNamespaces = new HashMap<>();

        // disable imports
        OntDocumentManager dm = OntDocumentManager.getInstance();
        dm.setProcessImports(false);

        // prepare spec
        // OntModelSpec spec = new OntModelSpec(ModelFactory.createMemModelMaker(), null, new CustomReasonerFactory(), ProfileRegistry.OWL_LANG);

        // create model
        model = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM_RDFS_INF);
    }

    /**
     * @param filePath
     */
    public ContextBuilder addOntology(String filePath) {
        RDFParser.source(filePath).parse(model);
        return this;
    }

    /**
     * @param ns
     */
    public ContextBuilder excludeNamespace(String ns) {
        excludedNamespaces.add(ns);
        return this;
    }

    public ContextBuilder addNamespace(String prefix, String namespace) {
        additionalNamespaces.put(prefix, namespace);
        return this;
    }

    /**
     * build
     * @return
     */
    public Context build() {
        return new Context(model, defaultNamespace, additionalNamespaces, excludedNamespaces);
    }

}
